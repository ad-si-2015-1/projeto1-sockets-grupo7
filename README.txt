O arquivo "App1.zip" contem o jar para execução de um Agente por execução. Vc pode executar ele várias vezes até executar todos os agentes que quiser.

Para isso, basta extrai-lo e executar o jar da seguinte forma: java -jar App1.jar
Será pedido alguns parametros. O padrão para o input da subrede é, por exemplo, : 192.168.1.
E em seguida será requisitado o ip da aranha.

O arquivo App1.ant também está disponível.

Caso queira rodar o código diretamente, você pode instanciar os Agentes que quiser dentro da classe Aplicação, no método main e passar os parametros como subrede, porta, etc.


