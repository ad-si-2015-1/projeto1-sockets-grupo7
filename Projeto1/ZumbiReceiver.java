import java.net.DatagramSocket;
import java.net.InetAddress;


public class ZumbiReceiver extends Agentes implements IReceiver{

	public ZumbiReceiver(String subRede, int porta, InetAddress ipAranha, DatagramSocket socketUDP) {
		super(Tipos.Zumbi, subRede, porta, ipAranha, socketUDP);
	}



	@Override
	public void run() {

		try {

			while(true){

				Mensagem msg = this.lerMsg();

				classificarMsg(msg);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void classificarMsg(Mensagem msg) {

		switch (msg.getTipo()) {
		case Aranha:
			//Se ele recebeu o ultimo agente encontrado pela Aranha, então prossegue com a caça!
			if(msg.getMensagem().equals("Ultimo agente encontrado")){

				Mensagem msgRetorno = new Mensagem();
				msgRetorno.setTipo(Tipos.Zumbi);
				msgRetorno.setMensagem("Mordida");

				try {
					this.enviarMsg(msgRetorno, msg.getUltimoAgente());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

			else if(msg.getMensagem().equals("Vc e agente?")){

				Mensagem msgRetorno = new Mensagem();
				msgRetorno.setTipo(Tipos.Zumbi);
				msgRetorno.setMensagem("Sou um agente");

				try {
					this.enviarMsg(msgRetorno, this.getIpAranha());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

			break;

		case Cacador:
			if(msg.getMensagem().equals("Vc esta preso")){

				Mensagem msgRetorno = new Mensagem();
				msgRetorno.setTipo(Tipos.Zumbi);
				msgRetorno.setMensagem("Zumbi nativo nao pode ser preso");

				try {
					this.enviarMsg(msgRetorno, msg.getIpRemetente());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}


			break;

		case Curador:
			if(msg.getMensagem().equals("Vc esta curado")){

				Mensagem msgRetorno = new Mensagem();
				msgRetorno.setTipo(Tipos.Zumbi);
				msgRetorno.setMensagem("Zumbi nativo nao pode ser curado");

				try {
					this.enviarMsg(msgRetorno, msg.getIpRemetente());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}


			break;

		default:
			if(!msg.getMensagem().equals("Me envie um agente")){
				Mensagem msgRetorno = new Mensagem();
				msgRetorno.setTipo(Tipos.Zumbi);
				msgRetorno.setMensagem("Nao sou o que vc esta querendo ou procurando");

				try {
					this.enviarMsg(msgRetorno, msg.getIpRemetente());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			break;
		}

	}

}


