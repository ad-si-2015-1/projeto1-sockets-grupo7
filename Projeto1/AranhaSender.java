import java.net.DatagramSocket;
import java.net.InetAddress;
/**
 * @author rychard
 *
 */
public class AranhaSender extends Agentes{

	public AranhaSender(String subRede, int porta, InetAddress ipAranha, DatagramSocket socketUDP) {
		super(Tipos.Aranha, subRede, porta, ipAranha, socketUDP);
	}

	@Override
	public void run() {
		try {
			int ipAux = 1;

			Mensagem msg = new Mensagem();
			msg.setMensagem("Vc e agente?");
			msg.setTipo(Tipos.Aranha);

			for (ipAux = 1; ipAux <= 254; ipAux++) {

				this.enviarMsg(msg, InetAddress.getByName(getSubRede()+ipAux));

				//Thread.sleep(350);

				if(ipAux == 254){
					ipAux = 1;

					Thread.sleep(2000);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
}
