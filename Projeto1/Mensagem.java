import java.io.Serializable;
import java.net.InetAddress;


public class Mensagem implements Serializable{
	


	

	/**
	 * 
	 */
	private static final long serialVersionUID = -1271518981833717471L;
	//Tipo do Agente
	private Tipos tipo;
	//Mensagem enviada
	private String mensagem;
	//Ip usado para enviar o ultimo agente da Aranha
	private InetAddress ultimoAgente;
	//Ip do remetente
	private InetAddress ipRemetente;
	
	public Tipos getTipo() {
		return tipo;
	}
	public void setTipo(Tipos tipo) {
		this.tipo = tipo;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	public InetAddress getUltimoAgente() {
		return ultimoAgente;
	}
	public void setUltimoAgente(InetAddress ultimoAgente) {
		this.ultimoAgente = ultimoAgente;
	}
	public InetAddress getIpRemetente() {
		return ipRemetente;
	}
	public void setIpRemetente(InetAddress ipRemetente) {
		this.ipRemetente = ipRemetente;
	}

}
