import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * 
 */

/**
 * @author Rodrigo Figueira
 *
 */
public class CuradorSender extends Agentes {

	public CuradorSender(String subRede, int porta, InetAddress ipAranha, DatagramSocket socketUDP) {
		super(Tipos.Aranha, subRede, porta, ipAranha, socketUDP);
	}

	@Override
	public void run() {
		try {
			
			Mensagem msg = new Mensagem();
			msg.setMensagem("Me envie um agente");
			msg.setTipo(Tipos.Curador);
			
			while(true) {

				this.enviarMsg(msg, this.getIpAranha());
				
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
}
