/**
 * 
 */

/**
 * @author Rodrigo Figueira
 *
 */

public enum Tipos {
	Aranha(0),Cacador(1), Curador(2), Respondedor(3), Questionador(4),Zumbi(5);



	private int valorTipos;

	Tipos(int valor){
		valorTipos=valor;

	}
	public int getValorTipos(){
		return valorTipos;
		
	}
}

