import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Scanner;

import sun.management.Agent;


public class Aplicacao {

	public static void main(String[] args) {
		// Este permite um teste mais rapido
		try {
			
			DatagramSocket socketUDP = new DatagramSocket(1200);
			String subrede = "192.168.25.";
			
			Agentes aranhaReceiver = new AranhaReceiver(subrede, 1200, InetAddress.getByName("192.168.25.31"), socketUDP);
			Agentes aranhaSender = new AranhaSender(subrede, 1200, InetAddress.getByName("192.168.25.31"), socketUDP);
			
			Agentes cacadorReceiver = new CacadorReceiver(subrede, 1200, InetAddress.getByName("192.168.25.31"), socketUDP);
			Agentes cacadorSender = new CacadorSender(subrede, 1200, InetAddress.getByName("192.168.25.31"), socketUDP);
			
			Agentes curadorReceiver = new CuradorReceiver(subrede, 1200, InetAddress.getByName("192.168.25.31"), socketUDP);
			Agentes curadorSender = new CacadorSender(subrede, 1200, InetAddress.getByName("192.168.25.31"), socketUDP);
			
			Agentes zumbiReceiver = new ZumbiReceiver(subrede, 1200, InetAddress.getByName("192.168.25.31"), socketUDP);
			Agentes zumbiSender = new ZumbiSender(subrede, 1200, InetAddress.getByName("192.168.25.31"), socketUDP);
			
			Agentes questReceiver = new QuestionadorReceiver(subrede, 1200, InetAddress.getByName("192.168.25.31"), socketUDP);
			Agentes questSender = new QuestionadorSender(subrede, 1200, InetAddress.getByName("192.168.25.31"), socketUDP);
			
			Agentes respReceiver = new RespondedorReceiver(subrede, 1200, InetAddress.getByName("192.168.25.31"), socketUDP);
			Agentes respSender = new RespondedorSender(subrede, 1200, InetAddress.getByName("192.168.25.31"), socketUDP);
			
			new Thread(aranhaReceiver).start();
			new Thread(aranhaSender).start();
			new Thread(cacadorReceiver).start();
			new Thread(cacadorSender).start();
			new Thread(curadorReceiver).start();
			new Thread(curadorSender).start();
			new Thread(zumbiReceiver).start();
			new Thread(zumbiSender).start();
			new Thread(questReceiver).start();
			new Thread(respReceiver).start();
			new Thread(respSender).start();
		} catch (Exception e) {
			// TODO: handle exception
		}
		//Modo onde vc pode escolher as classes que quer executar
		/*
		try {
			boolean naoSair = true;
			while(naoSair){
				DatagramSocket socketUDP = new DatagramSocket(1200);
				Scanner entrada = new Scanner(System.in);
				System.out.println("Digite o nome do agente ou 'sair' para Sair! ");
				System.out.println("(Opções: aranha, zumbi, cacador, questionador respondedor e curador) ");
				String nome = entrada.nextLine();

				System.out.println("Agora digite a subRede! ");
				String subRede = entrada.nextLine();

				System.out.println("Agora digite o ip! ");
				String ip = entrada.nextLine();
				if(entrada.equals("sair")){
					naoSair = false;
					System.exit(0);
				}else if (nome.equals("aranha")){


					Agentes aranhaReceiver = new AranhaReceiver(subRede, 1200, InetAddress.getByName(ip), socketUDP);
					Agentes aranhaSender = new AranhaSender(subRede, 1200, InetAddress.getByName(ip), socketUDP);
					new Thread(aranhaReceiver).start();
					new Thread(aranhaSender).start();
				}else if (nome.equals("zumbi")){
					Agentes zumbiReceiver = new ZumbiReceiver (subRede, 1200, InetAddress.getByName(ip), socketUDP);
					Agentes zumbiSender = new ZumbiSender(subRede, 1200, InetAddress.getByName(ip), socketUDP);
					new Thread(zumbiReceiver).start();
					new Thread(zumbiSender).start();
				}else if (nome.equals("cacador")){
					Agentes cacadorReceiver = new CacadorReceiver (subRede, 1200, InetAddress.getByName(ip), socketUDP);
					Agentes cacadorSender = new CacadorSender(subRede, 1200, InetAddress.getByName(ip), socketUDP);
					new Thread(cacadorReceiver).start();
					new Thread(cacadorSender).start();
				}else if (nome.equals("curador")){
					Agentes curadorReceiver = new CuradorReceiver (subRede, 1200, InetAddress.getByName(ip), socketUDP);
					Agentes curadorSender = new CuradorSender(subRede, 1200, InetAddress.getByName(ip), socketUDP);
					new Thread(curadorReceiver).start();
					new Thread(curadorSender).start();
				}else if (nome.equals("questionador")){
					Agentes questionadorReceiver = new QuestionadorReceiver (subRede, 1200, InetAddress.getByName(ip), socketUDP);
					Agentes questionadorSender = new QuestionadorSender(subRede, 1200, InetAddress.getByName(ip), socketUDP);
					new Thread(questionadorReceiver).start();
					new Thread(questionadorSender).start();
				}else if (nome.equals("respondedor")){
					Agentes respondedorReceiver = new RespondedorReceiver (subRede, 1200, InetAddress.getByName(ip), socketUDP);
					Agentes respondedorSender = new RespondedorSender(subRede, 1200, InetAddress.getByName(ip), socketUDP);
					new Thread(respondedorReceiver).start();
					new Thread(respondedorSender).start();
				}
			}

		}
		catch (Exception e) {}
*/
	}
}
