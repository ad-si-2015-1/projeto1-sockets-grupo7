import java.net.DatagramSocket;
import java.net.InetAddress;


public class RespondedorReceiver extends Agentes implements IReceiver{

	private boolean preso;

	public RespondedorReceiver(String subRede, int porta, InetAddress ipAranha, DatagramSocket socketUDP) {
		super(Tipos.Respondedor, subRede, porta, ipAranha, socketUDP);
		this.preso = false;
	}

	@Override
	public void run() {
		try {

			while(true){

				Mensagem msg = this.lerMsg();

				classificarMsg(msg);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void classificarMsg(Mensagem msg) {
		//variaveis auxiliares
		Double priNumero = null;
		Double segNumero = null;

		Mensagem msgRetorno = null;

		if(!this.getInfectado()){
			switch (msg.getTipo()) {
			//se eh zumbi seta infectado igual a TRUE
			case Zumbi:
				if(!msg.getMensagem().equals("Me envie um agente")){
					this.setInfectado(true);

					msgRetorno = new Mensagem();
					msgRetorno.setTipo(Tipos.Respondedor);
					msgRetorno.setMensagem("Agora me tornei um zumbi, fui infectado!!!");

					try {
						this.enviarMsg(msgRetorno, msg.getIpRemetente());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				break;
				//se eh curador seta infectado igual a FALSE
			case Curador:
				if(!msg.getMensagem().equals("Me envie um agente"))
					this.setInfectado(false);
				break;
				//se eh Aranha pega o ultimo agente
			case Aranha:

				if(msg.getMensagem().equals("Ultimo agente encontrado")){

					msgRetorno = new Mensagem();
					msgRetorno.setTipo(Tipos.Respondedor);
					msgRetorno.setMensagem("Vc e um Questionador?");

					try {
						this.enviarMsg(msgRetorno, msg.getUltimoAgente());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

				else if(msg.getMensagem().equals("Vc e agente?")){

					msgRetorno = new Mensagem();
					msgRetorno.setTipo(Tipos.Respondedor);
					msgRetorno.setMensagem("Sou um agente");

					try {
						this.enviarMsg(msgRetorno, this.getIpAranha());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

				break;
				//se eh um Questionador pega a pergunta e calcula a resposta
			case Questionador:

				msgRetorno = new Mensagem();
				msgRetorno.setTipo(Tipos.Respondedor);

				if(msg.getMensagem().contains("Pergunta=")){
					priNumero = Double.parseDouble(msg.getMensagem().substring(9, 12));
					segNumero = Double.parseDouble(msg.getMensagem().substring(14, 17));	
					msgRetorno.setMensagem(msg.getMensagem()+"Resp="+String.valueOf(priNumero + segNumero));

					try {
						this.enviarMsg(msgRetorno, msg.getIpRemetente());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				break;

			default:
				if(!msg.getMensagem().equals("Me envie um agente")){
					msgRetorno = new Mensagem();
					msgRetorno.setTipo(Tipos.Respondedor);
					msgRetorno.setMensagem("Nao sou o que vc esta querendo ou procurando");

					try {
						this.enviarMsg(msgRetorno, msg.getIpRemetente());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				break;

			}
		}

		else if(!isPreso()){
			switch (msg.getTipo()) {
			case Aranha:

				if(msg.getMensagem().equals("Ultimo agente encontrado")){

					msgRetorno = new Mensagem();
					msgRetorno.setTipo(Tipos.Zumbi);
					msgRetorno.setMensagem("Mordida");

					try {
						this.enviarMsg(msgRetorno, msg.getUltimoAgente());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

				else if(msg.getMensagem().equals("Vc e agente?")){

					msgRetorno = new Mensagem();
					msgRetorno.setTipo(Tipos.Zumbi);
					msgRetorno.setMensagem("Sou um agente");

					try {
						this.enviarMsg(msgRetorno, this.getIpAranha());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

				break;

			case Curador:

				if(msg.getMensagem().equals("Vc esta Curado!")){

					this.setInfectado(false);

				}

				break;
			case Cacador:

				if(msg.getMensagem().equals("Vc esta preso, banido!")){

					this.setPreso(true);

				}

				break;
			default:
				if(!msg.getMensagem().equals("Me envie um agente")){
					msgRetorno = new Mensagem();
					msgRetorno.setTipo(Tipos.Zumbi);
					msgRetorno.setMensagem("Mordida");

					try {
						this.enviarMsg(msgRetorno, msg.getIpRemetente());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				break;
			}
		}

		else{
			msgRetorno = new Mensagem();
			msgRetorno.setTipo(Tipos.Zumbi);
			msgRetorno.setMensagem("Estou preso e não posso responder mais");
			try {
				this.enviarMsg(msgRetorno, msg.getIpRemetente());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	public boolean isPreso() {
		return preso;
	}

	public void setPreso(boolean preso) {
		this.preso = preso;
	}
}