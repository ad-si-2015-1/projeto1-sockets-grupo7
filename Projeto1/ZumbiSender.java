import java.net.DatagramSocket;
import java.net.InetAddress;

public class ZumbiSender extends Agentes {

	public ZumbiSender(String subRede, int porta, InetAddress ipAranha, DatagramSocket socketUDP) {
		super(Tipos.Aranha, subRede, porta, ipAranha, socketUDP);
	}

	@Override
	public void run() {
		try {

			Mensagem msg = new Mensagem();
			msg.setMensagem("Me envie um agente");
			msg.setTipo(Tipos.Zumbi);

			while(true) {

				this.enviarMsg(msg, this.getIpAranha());

				Thread.sleep(2000);

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		


	}

}
