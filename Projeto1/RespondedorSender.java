import java.net.DatagramSocket;
import java.net.InetAddress;


public class RespondedorSender extends Agentes{

	public RespondedorSender(String subRede, int porta, InetAddress ipAranha, DatagramSocket socketUDP) {
		super(Tipos.Aranha, subRede, porta, ipAranha, socketUDP);
	}

	@Override
	public void run() {
		try {

			Mensagem msg = new Mensagem();
			msg.setMensagem("Me envie um agente");
			msg.setTipo(Tipos.Respondedor);

			while(true) {
				if(!this.getInfectado()){
					this.enviarMsg(msg, this.getIpAranha());

					Thread.sleep(2000);


				}

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
