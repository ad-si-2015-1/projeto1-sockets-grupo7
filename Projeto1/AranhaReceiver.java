import java.net.DatagramSocket;
import java.net.InetAddress;


public class AranhaReceiver extends Agentes implements IReceiver{

	private InetAddress ultimoAgente;

	public AranhaReceiver(String subRede, int porta, InetAddress ipAranha, DatagramSocket socketUDP) {
		super(Tipos.Aranha, subRede, porta, ipAranha, socketUDP);
	}

	@Override
	public void run() {

		try {
			
			while(true){
			
				Mensagem msg = this.lerMsg();

				classificarMsg(msg);
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	
	public void classificarMsg(Mensagem msg) {
		
		Mensagem msgRetorno = null;

		switch (msg.getMensagem().trim()) {
		case "Sou um agente":
			
			this.setUltimoAgente(msg.getIpRemetente());
			
			break;
			
		case "Me envie um agente":
			
			msgRetorno = new Mensagem();
			msgRetorno.setTipo(Tipos.Aranha);
			
			if(this.getUltimoAgente() != null){
			
			msgRetorno.setUltimoAgente(ultimoAgente);
			msgRetorno.setTipo(Tipos.Aranha);
			msgRetorno.setMensagem("Ultimo agente encontrado");
			
			}
			
			else{
				msgRetorno.setMensagem("Nenhum agente encontrado ainda");
			}
			
			try {
				this.enviarMsg(msgRetorno, msg.getIpRemetente());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;

		}

	}

	public InetAddress getUltimoAgente() {
		return ultimoAgente;
	}

	public void setUltimoAgente(InetAddress ultimoAgente) {
		this.ultimoAgente = ultimoAgente;
	}

}
