import java.io.*;
import java.net.*;

public abstract class Agentes implements Runnable{
	
	//indica tipo do agente
	private Tipos tipo;
	//indica a subrede do Agente
	private String subRede;
	//indica a porta usada
	private int porta;
	//Endereco IP da aranha, fixo
	private InetAddress ipAranha;
	//indica se o agente foi infectado pelo zumbi
	private Boolean infectado = false;
	//Socket UDP usado para conexao
	private DatagramSocket socketUDP;
	
	
	public Agentes(Tipos tipo, String subRede, int porta, InetAddress ipAranha, DatagramSocket socketUDP) {
		this.setTipo(tipo);
		this.setSubRede(subRede);
		this.setPorta(porta);
		this.ipAranha = ipAranha;
		this.socketUDP = socketUDP;
	}

	public Mensagem lerMsg() throws IOException, ClassNotFoundException{
		
		DatagramPacket mensagemUDP = new DatagramPacket(new byte[1024], 1024);
		socketUDP.receive(mensagemUDP);

		Mensagem msg = this.converteParaMensagem(mensagemUDP);
		
		
		return msg;
	}
	
	public Boolean enviarMsg(Mensagem msg, InetAddress ipReceiver) throws IOException{
		String msgEnvio;
		
		msgEnvio = "Do agente= ";
		
		switch (this.getTipo()) {
			case Aranha:
				msgEnvio = msgEnvio + "Aranha ";
				break;
			
			case Cacador:
				msgEnvio = msgEnvio + "Cacador ";
				break;
			
			case Curador:
				msgEnvio = msgEnvio + "Curador ";
				break;
			
			case Questionador:
				msgEnvio = msgEnvio + "Questionador ";
				break;
			
			case Respondedor:
				msgEnvio = msgEnvio + "Respondedor ";
				break;
			
			case Zumbi:
				msgEnvio = msgEnvio + "Zumbi ";
				break;
		}
		
		msgEnvio = msgEnvio + "- IP="+String.valueOf(this.getPorta());
		
		msgEnvio = msgEnvio + " para o agente de IP="+String.valueOf(ipReceiver)+" Mensagem="+msg.getMensagem();
		
		byte[] msgBytes = new byte[1024];
		msgBytes = this.converteParaBytes(msg);
		
		socketUDP.send(new DatagramPacket(msgBytes, msgBytes.length, ipReceiver, this.getPorta()));
		
		System.out.print(!msgEnvio.contains("Aranha") ? msgEnvio + "\n" : "");
		
		return true;
	}
	
	public void infectarAgente(Agentes agente){
		if(getTipo().equals(Tipos.Zumbi) || getInfectado()){
			this.setInfectado(true);
		}
	}
	
	public synchronized byte[] converteParaBytes(Object objeto) throws IOException{
		
		ByteArrayOutputStream arraySaida = new ByteArrayOutputStream();
		ObjectOutputStream objos = new ObjectOutputStream(arraySaida);
		objos.writeObject(objeto);
		
		return arraySaida.toByteArray();
	}
	
	public synchronized Mensagem converteParaMensagem(DatagramPacket dadosMensagem) throws IOException, ClassNotFoundException{
		
		ByteArrayInputStream bai = new ByteArrayInputStream(dadosMensagem.getData());
		ObjectInputStream ois = new ObjectInputStream(bai);
		
		Mensagem mensagem = (Mensagem) ois.readObject();
		mensagem.setIpRemetente(dadosMensagem.getAddress());
		
		return mensagem;
		
	}
	
	/**
	 * @return the tipo
	 */
	public Tipos getTipo() {
		return tipo;
	}


	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(Tipos tipo) {
		this.tipo = tipo;
	}


	/**
	 * @return the ip
	 */
	public String getSubRede() {
		return this.subRede;
	}


	/**
	 * @param ip the ip to set
	 */
	public void setSubRede(String subRede) {
		this.subRede = subRede;
	}


	/**
	 * @return the infectado
	 */
	public Boolean getInfectado() {
		return infectado;
	}


	/**
	 * @param infectado the infectado to set
	 */
	public void setInfectado(Boolean infectado) {
		this.infectado = infectado;
	}


	public int getPorta() {
		return porta;
	}


	public void setPorta(int porta) {
		this.porta = porta;
	}

	public DatagramSocket getSocketUDP() {
		return socketUDP;
	}

	public void setSocketUDP(DatagramSocket socketUDP) {
		this.socketUDP = socketUDP;
	}

	public InetAddress getIpAranha() {
		return ipAranha;
	}

	public void setIpAranha(InetAddress ipAranha) {
		this.ipAranha = ipAranha;
	}
	
}
