import java.net.DatagramSocket;
import java.net.InetAddress;


public class QuestionadorReceiver extends Agentes implements IReceiver{

	private boolean preso;

	public QuestionadorReceiver(String subRede, int porta, InetAddress ipAranha, DatagramSocket socketUDP) {
		super(Tipos.Questionador, subRede, porta, ipAranha, socketUDP);
		this.preso = false;
	}

	@Override
	public void run() {
		try {

			while(true){

				Mensagem msg = this.lerMsg();

				classificarMsg(msg);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void classificarMsg(Mensagem msg) {
		//variaveis auxiliares
		Double priNumero;
		Double segNumero;
		Double resposta;

		Mensagem msgRetorno = null;

		if(!this.getInfectado()){

			switch (msg.getTipo()) {
			//se eh zumbi seta infectado igual a TRUE
			case Zumbi:
				if(!msg.getMensagem().equals("Me envie um agente"))
					this.setInfectado(true);
				break;
				//se eh curador seta infectado igual a FALSE
			case Curador:
				if(!msg.getMensagem().equals("Me envie um agente"))
					this.setInfectado(false);
				break;
				//se eh Respondedor, ou gera a pergunta ou valida a resposta
			case Respondedor:
				//gera numeros aleatorios
				long numero = (long) (100 + Math.random() * 899l);
				long numero2 = (long) (100 + Math.random() * 899l);

				msgRetorno = new Mensagem();
				msgRetorno.setTipo(Tipos.Questionador);
				if(!msg.getMensagem().equals("Me envie um agente")){
					//se estiver infectado nao responde
					if(this.getInfectado()){
						msgRetorno.setMensagem("Agente:Questionador Infectado");
					}
					else{
						//se pergunta se eh Questionador, ja envia uma pergunta
						if(msg.getMensagem().contains("Vc e um Questionador?")){
							msgRetorno.setMensagem("Pergunta="+String.valueOf(numero) +"+"+String.valueOf(numero2));
						}
						//se contem Resposta ja valida, se esta correta ou nao
						else if(msg.getMensagem().contains("Resp=")){
							priNumero = Double.parseDouble(msg.getMensagem().substring(9, 12));
							segNumero = Double.parseDouble(msg.getMensagem().substring(14, 17));
							resposta = Double.parseDouble(msg.getMensagem().substring(23));

							if((priNumero+segNumero) == resposta){
								msgRetorno.setMensagem("Resposta Correta!");
							}
							else{						
								msgRetorno.setMensagem("Resposta Incorreta!");
							}
						}
					}

					try {
						this.enviarMsg(msgRetorno, msg.getIpRemetente());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				break;

			case Aranha:

				if(msg.getMensagem().equals("Vc e agente?")){

					msgRetorno = new Mensagem();
					msgRetorno.setTipo(Tipos.Questionador);
					msgRetorno.setMensagem("Sou um agente");

					try {
						this.enviarMsg(msgRetorno, this.getIpAranha());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

				break;

			default:

				msgRetorno = new Mensagem();
				msgRetorno.setTipo(Tipos.Questionador);
				msgRetorno.setMensagem("Nao sou o que vc esta querendo ou procurando");

				try {
					this.enviarMsg(msgRetorno, msg.getIpRemetente());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				break;

			}
		}

		else if(!isPreso()){
			switch (msg.getTipo()) {
			case Aranha:

				if(msg.getMensagem().equals("Ultimo agente encontrado")){

					msgRetorno = new Mensagem();
					msgRetorno.setTipo(Tipos.Zumbi);
					msgRetorno.setMensagem("Mordida");

					try {
						this.enviarMsg(msgRetorno, msg.getUltimoAgente());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

				else if(msg.getMensagem().equals("Vc e agente?")){

					msgRetorno = new Mensagem();
					msgRetorno.setTipo(Tipos.Zumbi);
					msgRetorno.setMensagem("Sou um agente");

					try {
						this.enviarMsg(msgRetorno, this.getIpAranha());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

				break;

			case Curador:

				if(msg.getMensagem().equals("Vc esta Curado!")){

					this.setInfectado(false);

				}

				break;
			case Cacador:

				if(msg.getMensagem().equals("Vc esta preso, banido!")){

					this.setPreso(true);

				}

				break;
			default:
				if(!msg.getMensagem().equals("Me envie um agente")){
					msgRetorno = new Mensagem();
					msgRetorno.setTipo(Tipos.Zumbi);
					msgRetorno.setMensagem("Mordida");

					try {
						this.enviarMsg(msgRetorno, msg.getIpRemetente());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				break;
			}
		}

		else{
			if(!msg.getMensagem().equals("Me envie um agente")){
				msgRetorno = new Mensagem();
				msgRetorno.setTipo(Tipos.Zumbi);
				msgRetorno.setMensagem("Estou preso e não posso responder mais");
				try {
					this.enviarMsg(msgRetorno, msg.getIpRemetente());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}

	public boolean isPreso() {
		return preso;
	}

	public void setPreso(boolean preso) {
		this.preso = preso;
	}
}

